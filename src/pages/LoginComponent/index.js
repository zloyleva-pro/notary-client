import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { FormInputComponent } from '../../components/FormInputComponent';
import { ButtonComponent } from '../../components/ButtonComponent';
import { ButtonsAuthWrapper } from '../../styledComonents/ButtonsAuthWrapper';
import { ColWrapper } from '../../styledComonents/ColWrapper';
import { H2Wrapper } from '../../styledComonents/H2Wrapper';
import { DescriptionWrapper } from '../../styledComonents/DescriptionWrapper';
import {
  loginThunkHandler,
  resetPasswordFetchAction,
  sendForgotPasswordAction,
} from '../../store/actions/authActionsCreators';
import { GuestContentComponent } from '../../components/GuestContentComponent';
import { ModalForgotPasswordComponent } from '../../components/ModalForgotPasswordComponent';
import { ModalResetPasswordComponent } from '../../components/ModalResetPasswordComponent';

const LoginComponent = () => {
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [showForgotPassword, setForgotPassword] = useState(false);
  const [showSendSMSCode, setShowSendSMSCod] = useState(false);

  const [smsCode, setSMSCodeHandler] = useState('');
  const [validCode, setValidCode] = useState(false);

  const dispatch = useDispatch();
  const history = useHistory();

  const loginFetchHandler = async () => {
    try {
      // Validate

      // fire action Login
      await dispatch(loginThunkHandler({
        phone,
        password,
      }));
      history.push('/profile/');
    } catch (e) {
      console.error(e);
    }
  };

  const sendForgotPassword = async (phoneNumber) => {
    try {
      console.log('sendForgotPassword', phoneNumber);
      // todo dispatch sendForgotPassword
      await dispatch(sendForgotPasswordAction(phoneNumber));
      // todo show modal for enter smsCode
      await setForgotPassword(false);
      await setShowSendSMSCod(true);
    } catch (e) {
      console.error(e);
    }
  };

  const resendHandler = async () => {
    await setForgotPassword(true);
    await setShowSendSMSCod(false);
  };

  const inputSMSCodeHandler = async (text) => {
    await setValidCode(false);
    await setSMSCodeHandler(text);
    if (/^\d{10}$/.test(text)) {
      await setValidCode(true);
    }
  };

  const resetPasswordHandler = async () => {
    try {
      console.log('resetPasswordHandler');
      await dispatch(resetPasswordFetchAction({ smsCode, password }));
      history.push('/profile/');
    } catch (e) {
      console.error(e);
    }
  };

  const changeInputHandler = (data) => {
    // eslint-disable-next-line default-case
    switch (data.key) {
      case 'phone':
        setPhone(data.value);
        break;
      case 'password':
        setPassword(data.value);
        break;
      case 'smsCode':
        inputSMSCodeHandler(data.value);
        break;
    }
  };

  return (
    <GuestContentComponent>
      <ColWrapper>
        <H2Wrapper>Увійти</H2Wrapper>
        <DescriptionWrapper>Будь ласка, заповніть всі поля.</DescriptionWrapper>
        <FormInputComponent labelText="Введіть номер телефону" id="phone" value={phone} changeValue={changeInputHandler} />
        <FormInputComponent
          labelText="Введіть пароль"
          id="password"
          type="password"
          value={password}
          changeValue={changeInputHandler}
        />
        <ButtonsAuthWrapper>
          <ButtonComponent id="login" onClick={loginFetchHandler}>Увійти</ButtonComponent>
          <ButtonComponent colors="secondary" onClick={() => setForgotPassword(true)}>Забули пароль?</ButtonComponent>
        </ButtonsAuthWrapper>
      </ColWrapper>

      {showForgotPassword && (
        <ModalForgotPasswordComponent
          onSend={sendForgotPassword}
          onClose={() => setForgotPassword(false)}
        />
      )}
      {
        showSendSMSCode && (
          <ModalResetPasswordComponent
            onReSend={resendHandler}
            onClose={() => setShowSendSMSCod(false)}
            onSend={resetPasswordHandler}
            setSMSCodeHandler={changeInputHandler}
            smsCode={smsCode}
            validCode={validCode}
            password={password}
            setPassword={changeInputHandler}
          />
        )
      }
    </GuestContentComponent>
  );
};

export { LoginComponent };
