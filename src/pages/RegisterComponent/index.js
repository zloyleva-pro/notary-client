import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { FormInputComponent } from '../../components/FormInputComponent';
import { ButtonComponent } from '../../components/ButtonComponent';
import { LinkButtonComponent } from '../../components/LinkButtonComponent';
import { ModalSendSMSCodeComponent } from '../../components/ModalSendSMSCodeComponent';
import { CongratsActivatePhoneComponent } from '../../components/CongratsActivatePhoneComponent';
import { ColWrapper } from '../../styledComonents/ColWrapper';
import { H2Wrapper } from '../../styledComonents/H2Wrapper';
import { DescriptionWrapper } from '../../styledComonents/DescriptionWrapper';
import { ButtonsAuthWrapper } from '../../styledComonents/ButtonsAuthWrapper';
import { GuestContentComponent } from '../../components/GuestContentComponent';
import { activateUserFetchAction, registerFetchAction } from '../../store/actions/authActionsCreators';

const RegisterComponent = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [showConfirmRegistration, setShowConfirmRegistration] = useState(false);
  const [showCongratsActivatePhone, setShowCongratsActivatePhone] = useState(false);

  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [phone, setPhone] = useState('');
  const [smsCode, setSMSCodeHandler] = useState('');

  const registrationHandler = async () => {
    try {
      await dispatch(registerFetchAction({ name, password, phone }));
      await setShowConfirmRegistration(true);
    } catch (e) {
      console.log(e.data, e.status);
    }
  };

  const confirmPhoneHandler = async () => {
    await dispatch(activateUserFetchAction({ smsCode }));
    await setSMSCodeHandler('');
    await setShowConfirmRegistration(false);
    await setShowCongratsActivatePhone(true);

    await setTimeout(async () => {
      console.log('setTimeout');
      await setShowCongratsActivatePhone(false);
      history.push('/profile');
    }, 1000);
  };

  const closeConfirmRegistrationWindow = async () => {
    await setSMSCodeHandler('');
    await setShowConfirmRegistration(false);
  };

  const changeInputHandler = (data) => {
    // eslint-disable-next-line default-case
    switch (data.key) {
      case 'phone':
        setPhone(data.value);
        break;
      case 'password':
        setPassword(data.value);
        break;
      case 'name':
        setName(data.value);
        break;
      case 'smsCode':
        setSMSCodeHandler(data.value);
        break;
    }
  };

  return (
    <GuestContentComponent>
      <ColWrapper>
        <H2Wrapper>Реєстрація</H2Wrapper>
        <DescriptionWrapper>Будь ласка, заповніть всі поля.</DescriptionWrapper>
        <FormInputComponent labelText="Введіть ім’я" id="name" value={name} changeValue={changeInputHandler} />
        <FormInputComponent labelText="Введіть пароль" id="password" type="password" value={password} changeValue={changeInputHandler} />
        <FormInputComponent labelText="Введіть номер телефону" id="phone" value={phone} changeValue={changeInputHandler} />

        <ButtonsAuthWrapper>
          <ButtonComponent onClick={registrationHandler}>Реєстрація</ButtonComponent>
          <LinkButtonComponent to="/login" colors="secondary">Увійти</LinkButtonComponent>
        </ButtonsAuthWrapper>

        {showConfirmRegistration && <ModalSendSMSCodeComponent onClose={closeConfirmRegistrationWindow} onSend={confirmPhoneHandler} setSMSCodeHandler={changeInputHandler} smsCode={smsCode} />}
        {showCongratsActivatePhone && <CongratsActivatePhoneComponent onClose={() => setShowCongratsActivatePhone(false)} />}
      </ColWrapper>
    </GuestContentComponent>
  );
};

export { RegisterComponent };
