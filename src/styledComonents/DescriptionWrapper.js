import styled from 'styled-components';

const DescriptionWrapper = styled.div`
  margin: 32px 0;
  text-align: center;
`;

export { DescriptionWrapper };
