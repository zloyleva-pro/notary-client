import styled from 'styled-components';

const ButtonsAuthWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export { ButtonsAuthWrapper };
