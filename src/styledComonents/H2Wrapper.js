import styled from 'styled-components';

const H2Wrapper = styled.h1`
  margin: 0px;
  text-align: center;
  font-weight: normal;
  font-size: 38px;
  line-height: 45px;
`;

export { H2Wrapper };
