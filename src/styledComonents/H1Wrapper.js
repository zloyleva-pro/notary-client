import styled from 'styled-components';

const H1Wrapper = styled.h1`
  margin: 32px 0;
  text-align: center;
  font-weight: normal;
  font-size: 48px;
  line-height: 45px;
`;

export { H1Wrapper };
