const theme = {
  primaryBgColor: '#C4C4C4',
  primaryBorderColor: '#C4C4C4',
  primaryTextColor: '#333333',

  secondaryBgColor: '#fff',
  secondaryBorderColor: '#C4C4C4',
  secondaryTextColor: '#333333',
};

export { theme };
