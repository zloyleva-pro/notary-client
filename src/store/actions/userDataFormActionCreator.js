import { USER_DATA_FORM_CHANGE_INPUT_VALUE, USER_DATA_FORM_CHANGE_STEP_VALUE } from '../constants';

const changeUserFormStepValue = (payload) => ({
  type: USER_DATA_FORM_CHANGE_STEP_VALUE,
  payload,
});

const changeUserFormInputValue = (payload) => ({
  type: USER_DATA_FORM_CHANGE_INPUT_VALUE,
  payload,
});

export {
  changeUserFormInputValue,
  changeUserFormStepValue,
};
