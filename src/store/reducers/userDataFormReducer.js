import { createReducer } from '@reduxjs/toolkit';

import { loadState } from '../../helpers';
import { USER_DATA_FORM_CHANGE_INPUT_VALUE, USER_DATA_FORM_CHANGE_STEP_VALUE } from '../constants';

// todo: get init from localStorage
const stateData = loadState();
const initState = {
  steps: [
    { id: 1, filled: true, active: false },
    { id: 2, filled: true, active: false },
    { id: 3, filled: false, active: true },
    { id: 4, filled: false, active: false },
    { id: 5, filled: false, active: false },
  ],
  name: null,
  lastName: null,
  patronymic: null,
  inn: null,
  passportSeries: null,
  passportNumber: null,
  passportIssued: null,
  passportScreens: [],
  ...stateData?.userDataForm,
};

const userDataForm = createReducer(initState, {
  [USER_DATA_FORM_CHANGE_STEP_VALUE]: (state, { payload }) => (
    {
      ...state,
      steps: state.steps.map((_) => (_.id === payload ? { ..._, active: true } : { ..._, active: false })),
    }
  ),
  [USER_DATA_FORM_CHANGE_INPUT_VALUE]: (state, { payload }) => ({
    ...state,
    [payload.key]: payload.value,
  }),
});

export { userDataForm };
