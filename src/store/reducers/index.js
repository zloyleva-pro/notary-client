import { combineReducers } from 'redux';

import { userReducer as user } from './userReducer';
import { appReducer as app } from './appReducer';
import { userDataForm } from './userDataFormReducer';

const rootReducer = combineReducers({
  app,
  user,
  userDataForm,
});

export { rootReducer };
