import React from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { FormInputComponent } from '../../FormInputComponent';
import { ButtonComponent } from '../../ButtonComponent';
import { StepWrapperComponent } from '../StepWrapperComponent';
import { ButtonWrapperComponent } from '../ButtonWrapperComponent';
import img from '../../../assets/svg/arrow-right.svg';
import { changeUserFormInputValue } from '../../../store/actions/userDataFormActionCreator';

const InputsWrapper = styled('div')`
  display: flex;
`;

const StepFourthComponent = () => {
  const dispatch = useDispatch();
  const userDataForm = useSelector((state) => state.userDataForm);
  const changeInputHandler = async (data) => {
    await dispatch(changeUserFormInputValue(data));
  };

  const fields = {
    passportSeries: 'passportSeries',
    passportNumber: 'passportNumber',
    passportIssued: 'passportIssued',
  };

  return (
    <StepWrapperComponent width={600}>
      <h2>Паспортні данні</h2>
      <InputsWrapper>
        <FormInputComponent labelText="Серія" value={userDataForm[fields.passportSeries]} id={fields.passportSeries} type="text" changeValue={changeInputHandler} mr={3} width={180} />
        <FormInputComponent labelText="Номер" value={userDataForm[fields.passportNumber]} id={fields.passportNumber} type="text" changeValue={changeInputHandler} flexGrow={1} />
      </InputsWrapper>
      <FormInputComponent labelText="Ким і коли виданий" value={userDataForm[fields.passportIssued]} id={fields.passportIssued} type="text" changeValue={changeInputHandler} textarea />
      <ButtonWrapperComponent>
        <ButtonComponent colors="secondary">
          Продовжити <img src={img} alt="" />
        </ButtonComponent>
      </ButtonWrapperComponent>
    </StepWrapperComponent>
  );
};

export { StepFourthComponent };
