import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { FormInputComponent } from '../../FormInputComponent';
import { ButtonComponent } from '../../ButtonComponent';
import { StepWrapperComponent } from '../StepWrapperComponent';
import { ButtonWrapperComponent } from '../ButtonWrapperComponent';
import img from '../../../assets/svg/arrow-right.svg';
import { changeUserFormInputValue } from '../../../store/actions/userDataFormActionCreator';

const StepThirdComponent = () => {
  const dispatch = useDispatch();
  const userDataForm = useSelector((state) => state.userDataForm);
  const changeInputHandler = async (data) => {
    await dispatch(changeUserFormInputValue(data));
  };
  return (
    <StepWrapperComponent>
      <h2>Ідентифікаційний код</h2>
      <FormInputComponent labelText="Код з десяти цифр" value={userDataForm.inn} id="inn" type="text" changeValue={changeInputHandler} />
      <ButtonWrapperComponent>
        <ButtonComponent colors="secondary">
          Продовжити <img src={img} alt="" />
        </ButtonComponent>
      </ButtonWrapperComponent>
    </StepWrapperComponent>
  );
};

export { StepThirdComponent };
