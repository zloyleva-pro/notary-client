import React, { useState } from 'react';
import styled from 'styled-components';

import { StepWrapperComponent } from '../StepWrapperComponent';
import { FormInputComponent } from '../../FormInputComponent';
import { DayPickerComponent } from '../../DayPickerComponent';
import calendar from '../../../assets/svg/calendar.svg';
import { ModalOverlayComponent } from '../../ModalOverlayComponent';

const ButtonWrapper = styled('button')`
  border: unset;
  background: unset;
  padding: 0;
  display: flex;
  outline: none;
  cursor:pointer;
`;

const StepSecondComponent = () => {
  const [showDatePicker, setShowDatePicker] = useState(false);
  return (
    <StepWrapperComponent>
      <h2>Дата народження</h2>
      <FormInputComponent
        labelText="День, місяць, рік."
        value={null}
        id="birthDay"
        type="text"
        changeValue={() => {}}
      >
        <ButtonWrapper onClick={() => setShowDatePicker(true)}>
          <img src={calendar} alt="" />
        </ButtonWrapper>
      </FormInputComponent>
      {showDatePicker && (
        <ModalOverlayComponent onClose={() => setShowDatePicker(false)}>
          <DayPickerComponent />
        </ModalOverlayComponent>
      )}
    </StepWrapperComponent>
  );
};

export { StepSecondComponent };
