import React from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { changeUserFormStepValue } from '../../store/actions/userDataFormActionCreator';

import { StepItemComponent } from './StepItemComponent';
import { StepFirstComponent } from './StepFirstComponent';
import { StepSecondComponent } from './StepSecondComponent';
import { StepThirdComponent } from './StepThirdComponent';
import { StepFourthComponent } from './StepFourthComponent';
import { StepFifthComponent } from './StepFifthComponent';

const StepsFillingDataWrapper = styled('div')`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-direction: column;
  flex: 1;
  position:relative;
`;

const StepCounterWrapper = styled('ul')`
  list-style-type: none;
  margin: 0;
  padding: 0;
  display: flex;
`;

const components = [
  { id: 1, component: (<StepFirstComponent />) },
  { id: 2, component: (<StepSecondComponent />) },
  { id: 3, component: (<StepThirdComponent />) },
  { id: 4, component: (<StepFourthComponent />) },
  { id: 5, component: (<StepFifthComponent />) },
];

const StepsFillingDataComponent = () => {
  const dispatch = useDispatch();
  const userDataForm = useSelector((state) => state.userDataForm);

  // eslint-disable-next-line no-return-await
  const setActiveStep = async (item) => await dispatch(changeUserFormStepValue(item));
  return (
    <StepsFillingDataWrapper>

      <h1>Етапи заповнення даних</h1>
      <p>Будь ласка, заповніть наступні поля.</p>

      <StepCounterWrapper>
        {
          userDataForm.steps.map((el, i) => (
            <StepItemComponent
              key={el.id}
              el={el}
              filled={el.filled}
              active={el.active}
              last={userDataForm.steps.length - 1 === i}
              setActiveStep={setActiveStep}
            />
          ))
        }
      </StepCounterWrapper>
      {
        components.find((_) => _.id === userDataForm.steps.filter((el) => el.active)[0].id).component || (<div className="alert">Something went wrong! Pls reload page</div>)
      }

    </StepsFillingDataWrapper>
  );
};

export { StepsFillingDataComponent };
