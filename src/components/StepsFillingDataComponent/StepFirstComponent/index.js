import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { FormInputComponent } from '../../FormInputComponent';
import { ButtonComponent } from '../../ButtonComponent';
import { StepWrapperComponent } from '../StepWrapperComponent';
import { ButtonWrapperComponent } from '../ButtonWrapperComponent';
import img from '../../../assets/svg/arrow-right.svg';
import { changeUserFormInputValue } from '../../../store/actions/userDataFormActionCreator';

const StepFirstComponent = () => {
  const dispatch = useDispatch();
  const userDataForm = useSelector((state) => state.userDataForm);
  const changeInputHandler = async (data) => {
    await dispatch(changeUserFormInputValue(data));
  };

  const fields = {
    name: 'name',
    secondName: 'secondName',
    patronymic: 'patronymic',
  };

  return (
    <StepWrapperComponent>
      <h2>Прізвище, ім’я, по батькові</h2>

      <FormInputComponent labelText="Прізвище" value={userDataForm[fields.secondName]} id={fields.secondName} type="text" changeValue={changeInputHandler} />
      <FormInputComponent labelText="Ім’я" value={userDataForm[fields.name]} id={fields.name} type="text" changeValue={changeInputHandler} />
      <FormInputComponent labelText="По батькові" value={userDataForm[fields.patronymic]} id={fields.patronymic} type="text" changeValue={changeInputHandler} />

      <ButtonWrapperComponent>
        <ButtonComponent colors="secondary">
          Продовжити <img src={img} alt="" />
        </ButtonComponent>
      </ButtonWrapperComponent>
    </StepWrapperComponent>
  );
};

export { StepFirstComponent };
