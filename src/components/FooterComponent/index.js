import React from 'react';

import { ContainerComponent } from '../ContainerComponent';

const FooterComponent = () => (
  <ContainerComponent>
    footer
  </ContainerComponent>
);

export { FooterComponent };
